# pink-slits

EPICS IOC to control PINK slits

## docker-composer.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-slits/slits:v1.0
    container_name: slits
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-slits/iocBoot/iocslt"
    command: "./slits.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/slits
        target: /EPICS/autosave
```
