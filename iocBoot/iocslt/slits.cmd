#!../../bin/linux-x86_64/slt

## You may have to change slt to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/slt.dbd"
slt_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords(db/slt.db,"BL=PINK,DEV=SLITS,MTOP=PHY:AxisH,MBOTTOM=PHY:AxisI,MLEFT=PHY:AxisF,MRIGHT=PHY:AxisG")

cd "${TOP}/iocBoot/${IOC}"
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=SLITS")

## Start any sequence programs
#seq sncxxx,"user=epics"
